<?php

require_once "Poligono.php";

class Rectangulo extends Poligono
{
    private $ancho;
    private $alto;

    public function __construct(int $ancho, int $alto)
    {
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    public function calcularArea()
    {
        return $this->ancho * $this->alto;
    }
}
