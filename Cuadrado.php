<?php
require_once "Poligono.php";

class Cuadrado extends Poligono
{
    protected $lado;

    public function __construct(int $l)
    {
        $this->lado = $l;
    }

    public function calcularArea()
    {
        return $this->lado ** 2;
    }
}
