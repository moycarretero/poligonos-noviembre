<?php

class Circulo extends Poligono
{
    private $radio;

    public function __construct(int $radio)
    {
        $this->radio = $radio;
    }

    public function calcularArea()
    {
        return pi() * $this->radio ** 2;
    }
}
